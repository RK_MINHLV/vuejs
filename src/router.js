import Vue from 'vue';
import Router from 'vue-router';
import Login from "./components/Login";
import BankAccountList from "./components/bank_account/List";
import BankAccountAdd from "./components/bank_account/Add";

Vue.use(Router);

export default new Router({
  routes: [
    { path: '/login', name: 'Login', component: Login },
    { path: '/', name: 'List', component: BankAccountList, meta: { auth: true } },
    { path: '/bank-account/add', name: 'Add', component: BankAccountAdd },
  ]
});
