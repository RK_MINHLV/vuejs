import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbvue/build/css/mdb.css';
import Vue from 'vue';
import App from './components/App';
import router from "./router";

Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth)) {
    if (localStorage.logged === false || !localStorage.logged) {
      // eslint-disable-next-line callback-return
      next({name:'Login'});
    } else {
      // eslint-disable-next-line callback-return
      next();
    }
  } else {
    // eslint-disable-next-line callback-return
    next();
  }
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
